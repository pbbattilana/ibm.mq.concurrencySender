package ibm.mq.Concurrency;

import com.ibm.mq.jms.MQConnectionFactory;
import com.ibm.mq.spring.boot.MQConfigurationProperties;
import com.ibm.mq.spring.boot.MQConnectionFactoryCustomizer;
import com.ibm.mq.spring.boot.MQConnectionFactoryFactory;
import java.util.List;
import javax.jms.DeliveryMode;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

@EnableJms
@org.springframework.context.annotation.Configuration
public class Configuration {
    @Bean("configurationProperties")
    @ConfigurationProperties("con")
    public MQConfigurationProperties configurationProperties(){
        return new MQConfigurationProperties();
    }
    
    @Bean("MQConnectionFactoryOne")
    public MQConnectionFactory MQConnectionFactoryOne(
            @Qualifier("configurationProperties") MQConfigurationProperties configurationProperties,
            ObjectProvider<List<MQConnectionFactoryCustomizer>> factoryCustomizers
    ){
        return new MQConnectionFactoryFactory(
                configurationProperties,
                factoryCustomizers.getIfAvailable()
        ).createConnectionFactory(MQConnectionFactory.class);
    }
    
    @Bean("jmsTemplate")
    public JmsTemplate jmsTemplate(@Qualifier("MQConnectionFactoryOne") MQConnectionFactory cf){
        JmsTemplate template = new JmsTemplate(cf);
        template.setDeliveryMode(DeliveryMode.PERSISTENT);
        template.setDeliveryPersistent(true);
        template.setPubSubDomain(false);
        return template;
    }
}
