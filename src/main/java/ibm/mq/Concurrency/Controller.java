package ibm.mq.Concurrency;

import java.util.UUID;
import javax.jms.JMSException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("sender")
public class Controller {
    @Autowired
    @Qualifier("jmsTemplate")
    private JmsTemplate template;
    
    @Value("${con.queueName}")
    private String queueName;
    
    @PostMapping
    private ResponseEntity<String> sender() throws JMSException {
        UUID uuid = UUID.randomUUID();;
        for (int i = 0; i < 1000; i++) {
            log.info("Enviando uuid: '{}' a la cola", uuid);
            template.convertAndSend(queueName, uuid);
            uuid =  UUID.randomUUID();
        }

        return new ResponseEntity(uuid, HttpStatus.ACCEPTED);
    }
}
